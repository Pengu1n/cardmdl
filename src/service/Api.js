import axios from 'axios';
const instance = axios.create();

instance.defaults.baseURL = 'http://localhost:5000/api/';

instance.defaults.headers = {
    "Content-Type": "application/json",
}

export default {
    showList() {
        return instance.get('users/');
    },

    login(body) {
        console.log(body);
        return instance.post('users/login', {
            email: body.email,
            password: body.password
        })
    }
}